(function ($) {
  (function ($) {
    Drupal.behaviors.flexsliderViewsLazyload = {
      attach: function (context, settings) {
        var $views = $(context).find('.flexslider-lazyload');

        if ($views.length == 0 && $(context).hasClass('flexslider-lazyload')) {
          $views = $(context);
        }

        if ($views.length) {
          $views.each(function () {
            var $view = $(this);
            var $flexslider = $view.find('.flexslider');
            var flexslider = $flexslider.data('flexslider');

            flexslider.vars.after = function () {
              if (flexslider.count - flexslider.currentSlide <= 3) {
                $view.find('.pager-load-more a').click();
              }
            }
          });
        }
      }
    };
  }(jQuery));

  $(document).bind('views_load_more.new_content', function (event) {
    var $view = $(event.target);

    if ($view.hasClass('flexslider-lazyload')) {
      var $flexslider = $view.find('.flexslider');
      var flexslider = $flexslider.data('flexslider');

      $flexslider.find('.slides > li:nth-child(n+' + (flexslider.count + 1) + ')').each(function () {
        flexslider.addSlide(this);
        $(this).css({
          'display': 'block',
          'zIndex': 1,
          'opacity': 0
        });
      });
    }
  });
}(jQuery));
